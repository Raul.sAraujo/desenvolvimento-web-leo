//Chamar a biblioteca do express
const express = require("express");

const app = express();

//Chamando a abertura do meu serviço
//Criação do servidor express
app.listen(8080, () => console.log("O server esta ativo na porta 8080"));

let nome = "Raul Araujo"

//Criação da primeira rota /
app.get("/", (req, res) => {
    res.send(`<h1>Olá ${nome}</h1>`)
});

app.post("/getHTML", (req, res) => {
    let nome = req.body.nome;

    res.send(`<h1>Olá ${nome}</h1>`)
    // console.log(`Olá ${nome}`)
})
